package com.example.todoer;

import org.junit.jupiter.api.*;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.http.*;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class RetrofitTests {

    interface TodoerApi {
        @POST("/{user}/todo")
        Call<IdResponse> createTodo(@Path("user") String user, @Body NewTodoRequest req);

        @GET("/{user}/todo")
        Call<List<Todo>> getTodos(@Path("user") String user);

        @DELETE("/{user}/todo/{id}")
        Call<Void> markDone(@Path("user") String user, @Path("id") long id);
    }

    static TodoerApplication app;
    static TodoerApi api;

    @BeforeAll
    static void init() {
        app = new TodoerApplication();
        app.start(0);
        api = new Retrofit.Builder()
                .baseUrl("http://localhost:" + app.getPort() + "/")
                .addConverterFactory(JacksonConverterFactory.create())
                .build()
                .create(TodoerApi.class);
    }

    @AfterAll
    static void teardown() {
        app.stop();
    }

    @BeforeEach
    void cleanupDb() {
        app.repo.deleteAll();
    }


    @Test
    void testCreate() throws IOException {
        int httpCode = api
                .createTodo("adam", new NewTodoRequest("buy milk"))
                .execute()
                .code();
        assertThat(httpCode).isEqualTo(200);
    }

    @Test
    void testCreateAndGet() throws IOException {
        //given
        api.createTodo("adam", new NewTodoRequest("buy milk")).execute();

        //when
        var todos = api.getTodos("adam").execute().body();

        //then
        assertThat(todos)
                .usingElementComparatorIgnoringFields("id")
                .containsExactlyInAnyOrder(
                        Todo.builder().user("adam").description("buy milk").build()
                );
    }

    @Test
    void testMarkDone() throws IOException {
        //given
        long buyMilkId = api
                .createTodo("adam", new NewTodoRequest("buy milk"))
                .execute()
                .body()
                .getId();

        //when
        api.markDone("adam", buyMilkId).execute();

        //then
        var todos = api.getTodos("adam").execute().body();
        assertThat(todos).isEmpty();
    }
}

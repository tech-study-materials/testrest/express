package com.example.todoer;

import feign.Feign;
import feign.Headers;
import feign.Param;
import feign.RequestLine;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.okhttp.OkHttpClient;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class FeignTests {

    interface TodoerApi {
        @RequestLine("POST /{user}/todo")
        @Headers("Content-Type: application/json")
        IdResponse createTodo(@Param("user") String user, NewTodoRequest req);

        @RequestLine("GET /{user}/todo")
        @Headers("Content-Type: application/json")
        List<Todo> getTodos(@Param("user") String user);

        @RequestLine("DELETE /{user}/todo/{id}")
        @Headers("Content-Type: application/json")
        void markDone(@Param("user") String user, @Param("id") long id);
    }

    static TodoerApplication app;
    static TodoerApi api;

    @BeforeAll
    static void init() {
        app = new TodoerApplication();
        app.start(0);
        api = Feign.builder()
                .client(new OkHttpClient())
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
                .target(TodoerApi.class, "http://localhost:" + app.getPort());
    }

    @AfterAll
    static void teardown() {
        app.stop();
    }

    @BeforeEach
    void cleanupDb() {
        app.repo.deleteAll();
    }

    @Test
    void testCreateAndGet() {
        //given
        api.createTodo("adam", new NewTodoRequest("buy milk"));

        //when
        var todos = api.getTodos("adam");

        //then
        assertThat(todos)
                .usingElementComparatorIgnoringFields("id")
                .containsExactlyInAnyOrder(
                        Todo.builder().user("adam").description("buy milk").build()
                );
    }

    @Test
    void testMarkDone() {
        //given
        long buyMilkId = api
                .createTodo("adam", new NewTodoRequest("buy milk"))
                .getId();

        //when
        api.markDone("adam", buyMilkId);

        //then
        var todos = api.getTodos("adam");
        assertThat(todos).isEmpty();
    }
}

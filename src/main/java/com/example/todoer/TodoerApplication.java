package com.example.todoer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpServer;
import express.Express;
import express.http.request.Request;
import express.http.response.Response;
import express.utils.MediaType;
import lombok.*;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicLong;

import static java.util.stream.Collectors.toUnmodifiableList;


public class TodoerApplication {

    public static void main(String[] args) {
        new TodoerApplication().start(8080);
    }

    Repo repo;
    Express express;
    final ObjectMapper jackson = new ObjectMapper();

    @SneakyThrows
    void start(int port) {
        System.out.println("Application starting...");

        repo = new InMemRepo();
        express = new Express();
        express.post("/:user/todo", this::createTodo);
        express.get("/:user/todo", this::getTodos);
        express.delete("/:user/todo/:id", this::markDone);

        CountDownLatch latch = new CountDownLatch(1); //https://github.com/Simonwep/java-express/issues/20
        express.listen(latch::countDown, port);
        latch.await();
        System.out.println("Application started @ http://localhost:" + getPort());
    }

    void stop() {
        System.out.println("Application stopping");

        express.stop();
        express = null;
        repo = null;
    }

    @SneakyThrows
    int getPort() {
        //reflection, bleh... https://github.com/Simonwep/java-express/issues/19
        Field httpServer = Express.class.getDeclaredField("httpServer");
        httpServer.trySetAccessible();
        return ((HttpServer) httpServer.get(express)).getAddress().getPort();
    }

    @SneakyThrows
    void createTodo(Request req, Response res) {
        long id = repo.create(
                req.getParam("user"),
                jackson.readValue(req.getBody(), NewTodoRequest.class)
                        .getDescription()
        );
        res.setContentType(MediaType._json);
        res.send(jackson.writeValueAsString(new IdResponse(id)));
    }

    @SneakyThrows
    void getTodos(Request req, Response res) {
        res.setContentType(MediaType._json);
        res.send(
                jackson.writeValueAsString(
                        repo.findByUser(req.getParam("user"))
                )
        );
    }

    void markDone(Request req, Response res) {
        repo.deleteById(Long.parseLong(req.getParam("id")));
        res.setContentType(MediaType._json);
        res.send("");
    }
}


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
class Todo {
    long id;
    String user;
    String description;
}

@Data
@AllArgsConstructor
@NoArgsConstructor
class NewTodoRequest {
    String description;
}

@Data
@AllArgsConstructor
@NoArgsConstructor
class IdResponse {
    long id;
}

interface Repo {
    long create(String user, String description);
    List<Todo> findByUser(String user);
    void deleteById(long id);
    void deleteAll();
}

class InMemRepo implements Repo {
    final AtomicLong sequence = new AtomicLong();
    List<Todo> todos = new ArrayList<>();

    @Override
    public long create(String user, String description) {
        long id = sequence.incrementAndGet();
        todos.add(new Todo(id, user, description));
        return id;
    }

    @Override
    public List<Todo> findByUser(String user) {
        return todos.stream()
                .filter(t -> user.equals(t.user))
                .collect(toUnmodifiableList());
    }

    @Override
    public void deleteById(long id) {
        todos.stream()
                .filter(t -> id == t.id)
                .findFirst()
                .ifPresent(t -> todos.remove(t));
    }

    @Override
    public void deleteAll() {
        todos.clear();
    }
}